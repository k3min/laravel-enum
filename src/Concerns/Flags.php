<?php

namespace Codification\Enum\Concerns
{
	/**
	 * @mixin \Codification\Enum\Enum
	 */
	trait Flags
	{
		/**
		 * @param int|string|string[] $value
		 *
		 * @return int
		 */
		public static function castFlags($value) : int
		{
			if (is_numeric($value))
			{
				return $value;
			}

			if (empty($value))
			{
				return 0;
			}

			if (!is_array($value))
			{
				$value = explode(',', $value);
			}

			/** @var int $result */
			$result = array_reduce($value, function (int $result, string $value) : int
				{
					return $result | static::parse($value)->value;
				}, 0);

			return $result;
		}

		/**
		 * @param self $enum
		 *
		 * @return bool
		 */
		public function has($enum) : bool
		{
			static::assertType($enum);

			return (($this->value & $enum->value) != 0);
		}

		/**
		 * @param self ...$enums
		 *
		 * @return $this
		 */
		public function set(...$enums) : self
		{
			foreach ($enums as $enum)
			{
				static::assertType($enum);

				$this->value |= $enum->value;
			}

			return $this;
		}

		/**
		 * @param self ...$enums
		 *
		 * @return $this
		 */
		public function remove(...$enums) : self
		{
			foreach ($enums as $enum)
			{
				static::assertType($enum);

				$this->value &= ~$enum->value;
			}

			return $this;
		}
	}
}