<?php

namespace Codification\Enum\Database\Eloquent\Contracts
{
	use \Codification\Enum\Enum;

	/**
	 * @property \Codification\Enum\Enum[] $enums
	 */
	interface HasEnums
	{
		/**
		 * @param string $key
		 *
		 * @return mixed|\Codification\Enum\Enum
		 */
		public function getAttributeValue($key);

		/**
		 * @param string                        $key
		 * @param mixed|\Codification\Enum\Enum $value
		 *
		 * @return mixed
		 */
		public function setAttribute($key, $value);

		/**
		 * @return string[]|\Codification\Enum\Enum[]
		 */
		public function getEnums() : array;

		/**
		 * @param int|string                     $value
		 * @param string|\Codification\Enum\Enum $enum
		 *
		 * @return \Codification\Enum\Enum
		 */
		public function asEnum($value, $enum) : Enum;

		/**
		 * @param \Codification\Enum\Enum $enum
		 *
		 * @return int|string
		 */
		public function fromEnum(Enum $enum);
	}
}