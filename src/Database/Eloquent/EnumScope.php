<?php

namespace Codification\Enum\Database\Eloquent
{
	use Codification\Enum\Enum;
	use Illuminate\Database\Eloquent\Scope;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Support\Str;

	class EnumScope implements Scope
	{
		/**
		 * @param \Illuminate\Database\Eloquent\Builder $builder
		 * @param \Illuminate\Database\Eloquent\Model   $model
		 *
		 * @return void
		 */
		public function apply(Builder $builder, Model $model)
		{
		}

		/**
		 * @param \Illuminate\Database\Eloquent\Builder $builder
		 *
		 * @return void
		 */
		public function extend(Builder $builder) : void
		{
			/** @var \Codification\Enum\Database\Eloquent\Contracts\HasEnums $model */
			$model = $builder->getModel();

			foreach ($model->getEnums() as $column => $type)
			{
				$name = 'where' . Str::studly($column) . 'Has';

				$builder->macro($name, function (Builder $builder, Enum $value, string $boolean = 'and') use ($column)
					{
						return $builder->whereEnumHas($column, $value, $boolean);
					});
			}
		}
	}
}