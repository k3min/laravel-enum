<?php

namespace Codification\Enum\Database\Eloquent
{
	use \Codification\Enum\Enum;

	/**
	 * @mixin \Codification\Enum\Database\Eloquent\Contracts\HasEnums
	 * @mixin \Illuminate\Database\Eloquent\Concerns\HasAttributes
	 * @mixin \Illuminate\Database\Eloquent\Concerns\HasGlobalScopes
	 */
	trait HasEnums
	{
		/**
		 * @return void
		 */
		public static function bootHasEnums() : void
		{
			static::addGlobalScope(new EnumScope());
		}

		/**
		 * @param string $key
		 *
		 * @return mixed|\Codification\Enum\Enum
		 */
		public function getAttributeValue($key)
		{
			$value = parent::getAttributeValue($key);

			if ($value !== null && $this->isEnumAttribute($key))
			{
				return $this->asEnum($value, $this->enums[$key]);
			}

			return $value;
		}

		/**
		 * @param string                        $key
		 * @param mixed|\Codification\Enum\Enum $value
		 *
		 * @return mixed
		 */
		public function setAttribute($key, $value)
		{
			if ($value !== null && $this->isEnumAttribute($key))
			{
				$this->enums[$key]::assertType($value);

				$value = $this->fromEnum($value);
			}

			return parent::setAttribute($key, $value);
		}

		/**
		 * @return string[]|\Codification\Enum\Enum[]
		 */
		public function getEnums() : array
		{
			return $this->enums;
		}

		/**
		 * @param int|string                     $value
		 * @param string|\Codification\Enum\Enum $enum
		 *
		 * @return \Codification\Enum\Enum
		 */
		public function asEnum($value, $enum) : Enum
		{
			return $enum::cast($value);
		}

		/**
		 * @param \Codification\Enum\Enum $enum
		 *
		 * @return int|string
		 */
		public function fromEnum(Enum $enum)
		{
			return $enum->getValue();
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function isEnumAttribute(string $key) : bool
		{
			return isset($this->enums[$key]);
		}
	}
}