<?php

namespace Codification\Enum
{
	use Illuminate\Contracts\Validation\Rule;

	abstract class Enum implements \JsonSerializable
	{
		/**
		 * @var int|string
		 */
		protected $value;

		/**
		 * @var array[]
		 */
		protected static $cache = [];

		/**
		 * @var string[]
		 */
		protected static $hidden = [];

		/**
		 * @return int|string
		 */
		public function getValue()
		{
			return $this->value;
		}

		/**
		 * @param int|string $value
		 */
		protected function __construct($value)
		{
			if (!static::isValid($value))
			{
				throw new \UnexpectedValueException("Unexpected value '$value' for " . get_called_class());
			}

			$this->value = $value;
		}

		/**
		 * @param \Codification\Enum\Enum|int|string $enum
		 * @param bool                               $strict
		 *
		 * @return bool
		 */
		public function equals($enum, bool $strict = true) : bool
		{
			if (!is_object($enum))
			{
				$enum = static::cast($enum);
			}

			return ((!$strict || get_called_class() === get_class($enum)) && $this->value === $enum->value);
		}

		/**
		 * @param \Codification\Enum\Enum|int|string $enum
		 * @param bool                               $strict
		 *
		 * @return bool
		 */
		public function eq($enum, bool $strict = true) : bool
		{
			return $this->equals($enum, $strict);
		}

		/**
		 * @param int|string $value
		 * @param bool       $strict
		 *
		 * @return bool
		 */
		public static function isValid($value, bool $strict = true) : bool
		{
			return in_array($value, static::toArray(), $strict);
		}

		/**
		 * @param int|string $value
		 *
		 * @return $this
		 */
		public static function cast($value) : self
		{
			$class = static::class;

			foreach (class_uses_recursive($class) as $trait)
			{
				$method = 'cast' . class_basename($trait);

				if (method_exists($class, $method))
				{
					$value = forward_static_call([$class, $method], $value);
				}
			}

			return new static($value);
		}

		/**
		 * @param string $name
		 *
		 * @return $this
		 */
		public static function parse(string $name) : self
		{
			$values = static::toArray();

			if (!isset($values[$name]))
			{
				throw new \InvalidArgumentException("[$name] does not exist in " . get_called_class());
			}

			return new static($values[$name]);
		}

		/**
		 * @return int[]|string[]
		 */
		public static function values() : array
		{
			return array_values(static::toArray());
		}

		/**
		 * @return string[]
		 */
		public static function names() : array
		{
			return array_diff(array_keys(static::toArray()), static::$hidden);
		}

		/**
		 * @param bool $strict
		 *
		 * @return \Illuminate\Contracts\Validation\Rule
		 */
		public static function rule(bool $strict = true) : Rule
		{
			return new Validation\Rules\Enum(get_called_class(), $strict);
		}

		/**
		 * @param \Codification\Enum\Enum $enum
		 *
		 * @return void
		 */
		public static function assertType(Enum $enum) : void
		{
			$type  = get_called_class();
			$other = get_class($enum);

			if ($other !== $type)
			{
				throw new \InvalidArgumentException("$other !== $type");
			}
		}

		/**
		 * @return int[]|string[]
		 */
		public static function toArray() : array
		{
			$type = get_called_class();

			if (!isset(static::$cache[$type]))
			{
				try
				{
					$reflection = new \ReflectionClass($type);
				}
				catch (\ReflectionException $e)
				{
					throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
				}

				static::$cache[$type] = $reflection->getConstants();
			}

			return static::$cache[$type];
		}

		/**
		 * @param string $name
		 * @param array  $arguments
		 *
		 * @return $this
		 */
		public static function __callStatic(string $name, array $arguments) : self
		{
			return static::parse($name);
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->getValue();
		}

		/**
		 * @return int|string
		 */
		public function jsonSerialize()
		{
			return $this->getValue();
		}
	}
}