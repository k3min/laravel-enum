<?php

namespace Codification\Enum\Support\Providers
{
	use Codification\Enum\Enum;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Support\ServiceProvider;

	class EnumServiceProvider extends ServiceProvider
	{
		/**
		 * @return void
		 */
		public function boot() : void
		{
			Builder::macro('whereEnumHas', function (string $column, Enum $value, string $boolean = 'and') : Builder
				{
					/** @var \Illuminate\Database\Eloquent\Builder $builder */
					$builder = $this;
					$query   = $builder->getQuery();

					if (count((array)$query->joins) > 0)
					{
						$column = $builder->qualifyColumn($column);
					}

					$column = $query->grammar->wrap($column);
					$value  = $value->getValue();

					return $builder->whereRaw("$column & ? = ?", [$value, $value], $boolean);
				});
		}
	}
}