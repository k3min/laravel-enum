<?php

namespace Codification\Enum\Validation\Rules
{
	use Illuminate\Contracts\Validation\Rule;

	class Enum implements Rule
	{
		/**
		 * @var string|\Codification\Enum\Enum
		 */
		protected $enum;

		/**
		 * @var bool
		 */
		protected $strict = true;

		/**
		 * @param string|\Codification\Enum\Enum $enum
		 * @param bool                           $strict
		 */
		public function __construct($enum, bool $strict = true)
		{
			$this->enum   = $enum;
			$this->strict = $strict;
		}

		/**
		 * @param string     $attribute
		 * @param int|string $value
		 *
		 * @return bool
		 */
		public function passes($attribute, $value)
		{
			return $this->enum::isValid($value, $this->strict);
		}

		/**
		 * @return string
		 */
		public function message()
		{
			return 'The :attribute is invalid.';
		}
	}
}